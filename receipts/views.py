from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, CategoryForm, AccountForm


# Create a view of all the instances of the Receipt model
# put them in the context for the template
@login_required
def receipt_list(request):
    all_receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": all_receipts,
    }
    return render(request, "receipts/list.html", context)


@login_required
def receipt_create(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    all_categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_list": all_categories
    }
    return render(request, "categories/list.html", context)


@login_required
def account_list(request):
    all_accounts = Account.objects.filter(owner=request.user)
    context = {
        "account_list": all_accounts
    }
    return render(request, "accounts/list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = CategoryForm()

    context = {
        "form": form,
    }

    return render(request, "categories/create.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()

    context = {
        "form": form,
    }

    return render(request, "accounts/create.html", context)
